package br.up.edu.le02app03;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void mostrarAmarelo(View v){
        Toast.makeText(this, "Amarelo", Toast.LENGTH_LONG).show();
    }

    public void mostrarVermelho(View v){
        Toast.makeText(this, "Vermelho", Toast.LENGTH_LONG).show();
    }

    public void mostrarVerde(View v){
        Toast.makeText(this, "Verde", Toast.LENGTH_LONG).show();
    }

    public void mostrarAzul(View v){
        Toast.makeText(this, "Azul", Toast.LENGTH_LONG).show();
    }

}
